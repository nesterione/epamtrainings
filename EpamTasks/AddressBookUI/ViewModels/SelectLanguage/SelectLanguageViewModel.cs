﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using AddressBookUI.Commands;
using System.Collections.ObjectModel;
using AddressBookUI.Models;
using System.Globalization;
using System.Threading;
using System.Windows.Markup;

namespace AddressBookUI.ViewModels
{
    class SelectLanguageViewModel : ViewModelBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ObservableCollection<LanguageViewModel> LanguagesList { get; set; }

        private bool _closeView;

        private void Close(bool cl)
        {
            CloseView = cl;
        }

        public bool CloseView
        {
            get { return _closeView; }
            set
            {
                _closeView = value;
                OnPropertyChanged("CloseView");
            }
        }

        #region Constructor

        public SelectLanguageViewModel(List<Language> languages)
        {
            LanguagesList = new ObservableCollection<LanguageViewModel>();
            foreach (Language language in languages)
                LanguagesList.Add(new LanguageViewModel(language, Close));
        }

#endregion

        

    }
}
