﻿using AddressBookUI.Commands;
using AddressBookUI.Models;
using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;

namespace AddressBookUI.ViewModels
{
    class LanguageViewModel : ViewModelBase
    {
        public Language language;
        Action<bool> Close;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LanguageViewModel(Language language, Action<bool> Close)
        {
            this.language = language;
            this.Close = Close;
        }

        public string Description
        {
            get { return language.Description; }
            set
            {
                language.Culture = value;
                OnPropertyChanged("Description");
            }
        }

        public string Culture
        {
            get { return language.Culture; }
            set
            {
                language.Culture = value;
                OnPropertyChanged("Culture");
            }
        }


         #region Отмена

        private DelegateCommand cancelCommand;

        public ICommand CancelCommand
        {
            get
            {
                if (cancelCommand == null)
                {
                    cancelCommand = new DelegateCommand(Cancel);
                }
                return cancelCommand;
            }
        }

        private void Cancel()
        {
            Close(true);
            log.Info("Button Cancel Click");
        }

        #endregion

         private DelegateCommand okCommand;

        public ICommand OkCommand
        {
            get
            {
                if (okCommand == null)
                {
                    okCommand = new DelegateCommand(Ok);
                }
                return okCommand;
            }
        }

        private void Ok()
        {
            //Изменение текущей культуры, в соответствии в выбранной в выпадающем списке
            try
            {
                string culture = Culture;
                CultureInfo ci = new CultureInfo(culture);

                Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = ci;

                //Необходимо того что бы дынные отображались в соответствии с правилами
                //текущей культуры
                FrameworkElement.LanguageProperty.OverrideMetadata(
                typeof(FrameworkElement),
                new FrameworkPropertyMetadata(
                    XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

                log.Info("Selected language " + ci.DisplayName);
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); log.Fatal("Exeption " + ex); Close(true); }

            //Запуск авторизации
            AutorizationWindow mw = new AutorizationWindow();
            mw.Show();

            Close(true);
        }
    }
}