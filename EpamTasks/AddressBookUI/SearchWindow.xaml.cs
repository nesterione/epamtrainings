﻿using System.Windows;
using System.Linq.Expressions;
using System.Linq;
using System;
using AddressBook;
using DAL.Search;
using System.Windows.Controls;

namespace AddressBookUI
{
    /// <summary>
    /// Логика взаимодействия для SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        public SearchWindow()
        {
            InitializeComponent();
        }

        private void btn_simpleSearch_Click(object sender, RoutedEventArgs e)
        {
            ISearch search = new SimpleSearch(tb_query.Text);
            ((MainWindow)Owner).setQuery(search);
            this.Close();
        }

        private string readTextBox(TextBox tb)
        {
            string text = tb.Text;
            if (text.Length == 0)
                return null;
            else return text;
        }

        private void btn_advancedSearch_Click(object sender, RoutedEventArgs e)
        {
            string firstName = readTextBox(tb_firstName);
            string secondName = readTextBox(tb_secondName);
            string textNote = readTextBox(tb_textNote);
            string tag = readTextBox(tb_tag);
            string countryCode = readTextBox(tb_countryCode);
            string phoneNumber = readTextBox(tb_phoneNumber);
            string namePhone = readTextBox(tb_namePhone);

            QueryForAdvancedSearch query = new QueryForAdvancedSearch(firstName, secondName, textNote, tag, countryCode, phoneNumber, namePhone, null, null);
            ISearch search = new AdvancedSearch(query);
            ((MainWindow)Owner).setQuery(search);
            this.Close();
        }
    }
}
