﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AddressBook;

namespace AddressBookUI
{
    /// <summary>
    /// Interaction logic for EditNoteWindows.xaml
    /// </summary>
    public partial class EditNoteWindows : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EditNoteWindows()
        {
            InitializeComponent();
            log.Info("Initialized EditNoteWindows");
        }

        /// <summary>
        /// Индекс редактируемого номер телефона
        /// При добавлении нового телефона = Empty
        /// </summary>
        private Guid editedId = Guid.Empty;
        private Tag[] StringToTags(string stringTags)
        {
            string[] strTags = stringTags.Split();
            Tag[] tags = new Tag[strTags.Length];

            for (int i = 0; i < strTags.Length; i++)
            {
                tags[i] = new Tag(strTags[i]);
            }
            return tags;
        }
        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button OK Click");
            if (tb_tags.Text.Length>0)
            {
                try
                {
                    if (editedId.Equals(Guid.Empty))
                    {
                        log.Error("Create Note");
                        Note note = new Note(tb_note.Text, StringToTags(tb_tags.Text));
                        ((MainWindow)Owner).addNote(note);
                        Close();
                    }
                    else
                    {
                        log.Error("Update Note");
                        Note note = new Note(editedId, tb_note.Text, StringToTags(tb_tags.Text));
                        ((MainWindow)Owner).updateNote(note);
                        Close();
                    }
                }
                catch (Exception ex) { lb_errors.Content = ex.Message; log.Error("Exeption: ", ex); }
            }
            else
            {
                log.Error("Tags not found");
                lb_errors.Content =Properties.Resources.exp_noTags;
            }
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button Cancel Click");
            Close();
        }

        ///<summary>
        /// показывает данные объекта на форме
        /// </summary>
        /// <param name="phoneRecord"></param>
        public void showNote(Note note, Guid noteIndex)
        {
            editedId = noteIndex;
            tb_note.Text = note.text;
            tb_tags.Text = note.tagsString;
        }
    }
}