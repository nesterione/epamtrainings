﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressBookUI.UserControls
{
    /// <summary>
    /// Логика взаимодействия для DateTimePicker.xaml
    /// </summary>
    public partial class DateTimePicker : UserControl
    {
        public DateTimePicker()
        {
            InitializeComponent();
        }

        public DateTime? Date
        {
            get
            {
                try
                {
                    return new DateTime(c_date.SelectedDate.Value.Year,
                         c_date.SelectedDate.Value.Month,
                         c_date.SelectedDate.Value.Day,
                         c_time.Hour,
                         c_time.Minute,
                         c_time.Second);
                }
                catch { return null; }
            }
        }
    }
}
