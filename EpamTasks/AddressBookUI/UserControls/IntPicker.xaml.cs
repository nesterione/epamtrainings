﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AddressBookUI.UserControls
{
    /// <summary>
    /// Логика взаимодействия для IntPicker.xaml
    /// </summary>
    public partial class IntPicker : UserControl
    {
        public IntPicker()
        {
            InitializeComponent();
            Value = 0;
            ViewValue();
        }

        public int? MinValue { get; set; }
        public int? MaxValue { get; set; }
        public int Value { get; private set; }

        private void ViewValue()
        {
            tb_value.Text = Value.ToString();
            tb_value.SelectionStart = tb_value.Text.Length;
            tb_value.SelectAll();
        }

        private void btn_up_Click(object sender, RoutedEventArgs e)
        {
            if (MaxValue == null)
            { Value++; }
            else
                if (Value < MaxValue)
                { Value++; }

            ViewValue();
        }

        private void btn_down_Click(object sender, RoutedEventArgs e)
        {
            if (MinValue == null)
            { Value--; }
            else
                if (Value > MinValue)
                { Value--; }

            ViewValue();
        }

        private void tb_value_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                int tempValue = int.Parse(tb_value.Text);
                if (MinValue <= tempValue && tempValue <= MaxValue)
                    Value = tempValue;
                else
                    ViewValue();
            }
            catch { Value = 0; ViewValue(); }
        }

     
    }
}
