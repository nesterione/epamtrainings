﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AddressBook;

namespace AddressBookUI
{
    /// <summary>
    /// Interaction logic for EditPhoneWindow.xaml
    /// </summary>
    public partial class EditPhoneWindow : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EditPhoneWindow()
        {
            InitializeComponent();
            log.Error("Initialize EditPhoneWindow");
        }

        /// <summary>
        /// Индекс редактируемого номер телефона
        /// При добавлении нового телефона = Empty
        /// </summary>
        private Guid editedId = Guid.Empty;

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            log.Error("Button OK Click");
            try
            {
                PhoneRecord phoneRecord = null;
                PhoneNumber phoneNumber = new PhoneNumber(int.Parse(tb_countryCode.Text), long.Parse(tb_phone.Text));

                if (editedId.Equals(Guid.Empty))
                {
                    log.Error("Update Phone Record");
                    phoneRecord = new PhoneRecord(tb_namePhone.Text, phoneNumber);
                    ((MainWindow)Owner).addPhoneRecord(phoneRecord);
                    Close();
                }
                else
                {
                    log.Error("Create Phone Record");
                    phoneRecord = new PhoneRecord(editedId, tb_namePhone.Text, phoneNumber);
                    ((MainWindow)Owner).updatePhoneRecord(phoneRecord);
                    Close();
                }
            }
            catch (FormatException fex) { lb_errors.Content = Properties.Resources.exp_phoneError; log.Error("Exeption: ",fex); }
            catch (Exception ex) { lb_errors.Content = ex.Message; log.Error("Exeption: ", ex); }
        }

        /// <summary>
        /// показывает данные объекта на форме
        /// </summary>
        /// <param name="phoneRecord"></param>
        public void showPhoneNumber(PhoneRecord phoneRecord, Guid phoneIndex)
        {
            editedId = phoneIndex;
            tb_namePhone.Text = phoneRecord.name;
            tb_countryCode.Text = phoneRecord.phoneNumber.countryCode.ToString();
            tb_phone.Text = phoneRecord.phoneNumber.phone.ToString();
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            log.Error("Button Cancel Click");
            Close();
        }
    }
}
