﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddressBookUI.Models
{
    public class Language
    {
        public string Description { get; set; }
        public string Culture { get;  set; }

        public Language(string Description, string Culture)
        {
            this.Description = Description;
            this.Culture = Culture;
        }
    }
}
