﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Configuration;
using DAL.RepositoryInfo;
using System.Text.RegularExpressions;
using System.IO;

namespace AddressBookUI
{
    /// <summary>
    /// Логика взаимодействия для CreateRepository.xaml
    /// </summary>
    public partial class CreateRepository : Window
    {
        public CreateRepository()
        {
            InitializeComponent();
        }

        public bool IsSuccess { get; private set; }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<RepositoryInfo> repositories = new List<RepositoryInfo>();
            repositories.Add(new RepositoryInfo(RepositoryType.Memory, "Мемору", "Репозиторий в памяти"));
            repositories.Add(new RepositoryInfo(RepositoryType.XML, "XML", "XML репозиторий"));
            cb_repositoryType.ItemsSource = repositories;
            cb_repositoryType.SelectedIndex = 0;
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            string nameRepository = tb_nameRepository.Text;
            Regex regex = new Regex("['/\\:\\*\\?\"<>|]");

            // Проверка имени репозитория
            if (nameRepository.Length == 0 || regex.IsMatch(nameRepository))
            {
                MessageBox.Show("Недопустимое имя репозитория");
                return;
            }

            //Проверка существования пути, для сохранеия репозитория
            string repositoryPath = tb_repositoryPath.Text;
            if (!(new DirectoryInfo(repositoryPath)).Exists)
            {
                MessageBox.Show("Указанный, для хранения репозитория, католог не найден.");
                return;
            }

            try
            {
                // Загрузка файла конфигурации
                // и получения списка репозиториев
                // если возникла проблемма при загрузки секции, вевести ошибку
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                Configurations.AddressBookSectionGroup group = config.GetSectionGroup("addressBookGroup") as Configurations.AddressBookSectionGroup;
                Configurations.RepositoriesSection repositoriesSection = group.Repositories as Configurations.RepositoriesSection;
                
                if (repositoriesSection == null)
                {
                    MessageBox.Show("Возникла проблема при загрузки конфикурации.");
                    return;
                }

                bool sucessCreationRepository = false;
                Configurations.RepositoryElement newRepository = null;
                string fullPath = "";
                switch (((RepositoryInfo)cb_repositoryType.SelectedItem).RepositoryType)
                {
                    case RepositoryType.Memory:
                        fullPath = repositoryPath + "\\" + nameRepository + ".bin";
                        sucessCreationRepository = DAL.CreatorEmptyRepositories.CreteBinaryRepository(fullPath);
                        if (sucessCreationRepository)
                        {
                            newRepository = new Configurations.RepositoryElement(nameRepository, fullPath, "binary");
                        }
                        break;
                    case RepositoryType.XML:
                        fullPath = repositoryPath + "\\" + nameRepository + ".xml";
                        sucessCreationRepository = DAL.CreatorEmptyRepositories.CreateXMLRepository(fullPath);
                        if (sucessCreationRepository)
                        {
                            if (sucessCreationRepository)
                            {
                                string validationType = "";
                                if (rb_xsd.IsChecked == true)
                                {
                                    validationType = "xsd";
                                }
                                else
                                {
                                    validationType = "dtd";
                                }

                                newRepository = new Configurations.RepositoryElement(nameRepository, fullPath, "xml", validationType);
                            }
                        }
                        break;
                }


                if (!sucessCreationRepository)
                {
                    MessageBox.Show("Возникла ошибка при добавлении репозитория");
                    return;
                }

                repositoriesSection.Repositories.Add(newRepository);
                config.Save();
                IsSuccess = true;
                this.Close();

            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void cb_repositoryType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tb_descriptionRepository.Text = ((RepositoryInfo)(cb_repositoryType.SelectedItem)).Description;

            switch (((RepositoryInfo)(cb_repositoryType.SelectedItem)).RepositoryType)
            {
                case RepositoryType.Memory:
                    sp_memoryRepository.Visibility = System.Windows.Visibility.Visible;
                    sp_xmlRepository.Visibility = System.Windows.Visibility.Hidden;
                    break;
                case RepositoryType.XML:
                    sp_memoryRepository.Visibility = System.Windows.Visibility.Hidden;
                    sp_xmlRepository.Visibility = System.Windows.Visibility.Visible;
                    break;
            }
        }

        private void btn_browse_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            tb_repositoryPath.Text = dialog.SelectedPath;
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }

    public enum RepositoryType { Memory, XML, EF };

    class RepositoryInfo
    {
        public RepositoryType RepositoryType { get; private set; }
        public string Name { get; private set; }
        public string Description { get; private set; }

        public RepositoryInfo(RepositoryType RepositoryType, string Name, string Description)
        {
            this.RepositoryType = RepositoryType;
            this.Name = Name;
            this.Description = Description;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
