﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using AddressBook;
using DAL.Search;

namespace AddressBookUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xamll
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            //Инициализация источника данных 
            InitializeComponent();
            log.Info("Initialized MainWindow");
        }

        #region Properties

        public DAL.RepositoryInfo.IRepositoryInfo repositoryInfo = null;
        public DAL.IRepository<AddressRecord, Guid> addressRepository = null;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool IsAdmin { get; private set; }
        bool IsLoadedData = false;
        private bool IsRowEnitOn { get; set; }
        ISearch search = null;
        bool IsEditOn = false;

        private bool IsExistSearchQuery
        {
            get { return (search != null); }
        }
        #endregion

        private void resetQuery()
        {
            search = null;
            LoadData();
        }

        public void setQuery(ISearch search)
        {
            this.search = search;
            LoadData();
        }

        private void SelectRepository()
        {
            try
            {
                SelectRepository selectRepository_wnd = new SelectRepository();
                selectRepository_wnd.Owner = this;
                selectRepository_wnd.ShowDialog();
                if (repositoryInfo != null)
                {
                    addressRepository = repositoryInfo.CreateRepository();
                    lb_repositoryName.Content = repositoryInfo.Info;
                    LoadData();
                }
                else
                {
                    lb_repositoryName.Content = Properties.Resources.msg_repositoryNotSelected;
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); log.Error(" Exception - ", ex); }
        }

        private bool LoadData()
        {
            bool rezult = false;

            if (IsExistSearchQuery)
            {
                btn_searchCancel.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                btn_searchCancel.Visibility = System.Windows.Visibility.Hidden;
            }
            
            try
            {
                // Загрузка данных в таблицу.
                // Если пользователь указывал поисковый запрос,
                // то данные загружаются в соответствии с запросом
                // иначе идет загрузка всех записей
                if (IsExistSearchQuery)
                {
                    dg_adressView.ItemsSource = search.Search(addressRepository.Get());
                }
                else
                {
                    dg_adressView.ItemsSource = addressRepository.GetAll();
                }

                dg_adressView.Items.Refresh();
                IsLoadedData = true;
                rezult = true;

                log.Info("Loaded Repository - " + repositoryInfo.Info);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                IsLoadedData = false;
                dg_adressView.ItemsSource = null;
                lb_repositoryName.Content = Properties.Resources.msg_repositoryNotSelected;
                log.Error(" Exception - ", ex);
            }
            return rezult;
        }

        private void deleteRecord(AddressRecord delitingRecord)
        {
            addressRepository.Delete(delitingRecord);
            LoadData();
        }

        public void CustamizationProgram(string userRole)
        {
            switch (userRole)
            {
                case "Admin":
                    IsAdmin = true;
                    btn_addRecord.Visibility = System.Windows.Visibility.Visible;
                    btn_editRecordON.Visibility = System.Windows.Visibility.Visible;
                    break;
                default:
                    IsAdmin = false;
                    btn_addRecord.Visibility = System.Windows.Visibility.Hidden;
                    btn_editRecordON.Visibility = System.Windows.Visibility.Hidden;
                    break;
            }
        }

        private void ViewSelectedRow()
        {
            log.Info("Button View Click");
            try
            {
                CustomizeForView((AddressRecord)dg_adressView.SelectedItem);
            }
            catch { };
            //LoadData();
        }

        #region Правая панель

        private List<Note> notes = null;
        private List<PhoneRecord> phoneRecords = null;

        public void addNote(Note note)
        {
            notes.Add(note);
            dg_notes.Items.Refresh();
        }

        public void addPhoneRecord(PhoneRecord phoneNumber)
        {
            phoneRecords.Add(phoneNumber);
            dg_phoneNubers.Items.Refresh();
        }

        public void updatePhoneRecord(PhoneRecord phoneRecord)
        {
            bool found = false;

            for (int i = 0; i < phoneRecords.Count & !found; i++)
            {
                if (phoneRecords[i].id.Equals(phoneRecord.id))
                {
                    phoneRecords[i] = phoneRecord;
                    dg_phoneNubers.Items.Refresh();
                    found = true;
                }
            }

            if (!found)
            {
                if (MessageBox.Show("Не найден изменяемый номер телефон.\n Добавить как новый?",
                    "Внимание", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    addPhoneRecord(phoneRecord);
                }
            }
        }

        public void updateNote(Note note)
        {
            bool found = false;

            for (int i = 0; i < notes.Count & !found; i++)
            {
                if (notes[i].id.Equals(note.id))
                {
                    notes[i] = note;
                    dg_notes.Items.Refresh();
                    found = true;
                }
            }

            if (!found)
            {
                if (MessageBox.Show("Не найдена изменяемая заметка\n Добавить как новую", "Внимание",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    addNote(note);
                }
            }
        }

        /// <summary>
        /// Необходим при редактировании записи
        /// </summary>
        private Guid CurrentRecordId = Guid.Empty;
        private Guid addressRecordId = Guid.Empty;

        public void CustomizeForView(AddressRecord currentRecord)
        {
            CurrentRecordId = currentRecord.id;
            tb_firstName.Text = currentRecord.firstName;
            tb_secondName.Text = currentRecord.secondName;

            addressRecordId = currentRecord.id;

            copyListToList(currentRecord.phoneNumbers, out phoneRecords);
            copyListToList(currentRecord.notes, out notes);

            lb_lastChTime.Content = currentRecord.lastChTime;

            dg_phoneNubers.ItemsSource = phoneRecords;
            dg_notes.ItemsSource = notes;
        }

        public void CustomizeForEdit(AddressRecord currentRecord)
        {
            CustomizeForView(currentRecord);
            EditON();
        }

        private void ClearRightPanel()
        {
            tb_firstName.Text = "";
            tb_secondName.Text = "";
            dg_notes.ItemsSource = null;
            dg_notes.Items.Refresh();
            dg_phoneNubers.ItemsSource = null;
            dg_phoneNubers.Items.Refresh();
            lb_lastChTime.Content = "";
        }

        public void CustomizeForAdd()
        {
            ClearRightPanel();
            addressRecordId = Guid.Empty;
            phoneRecords = new List<PhoneRecord>();
            dg_phoneNubers.ItemsSource = phoneRecords;
            notes = new List<Note>();
            dg_notes.ItemsSource = notes;

           

            EditON();
        }

        private void copyListToList<T>(List<T> list, out List<T> newList)
        {
            newList = new List<T>();
            foreach (T listItem in list)
            {
                newList.Add(listItem);
            }
        }

        private void EditON()
        {
            IsRowEnitOn = true;
            dg_phoneNubers.Columns[2].Visibility = System.Windows.Visibility.Visible;
            dg_phoneNubers.Columns[3].Visibility = System.Windows.Visibility.Visible;
            dg_notes.Columns[2].Visibility = System.Windows.Visibility.Visible;
            dg_notes.Columns[3].Visibility = System.Windows.Visibility.Visible;
            btn_addNote.Visibility = System.Windows.Visibility.Visible;
            btn_addPhoneNumber.Visibility = System.Windows.Visibility.Visible;
            tb_firstName.IsReadOnly = false;
            tb_secondName.IsReadOnly = false;
            btn_editRecordON.Visibility = System.Windows.Visibility.Hidden;
            btn_editRecordOFF.Visibility = System.Windows.Visibility.Visible;
            IsEditOn = true;
        }

        private void EditOFF()
        {
            IsRowEnitOn = false;
            dg_phoneNubers.Columns[2].Visibility = System.Windows.Visibility.Hidden;
            dg_phoneNubers.Columns[3].Visibility = System.Windows.Visibility.Hidden;
            dg_notes.Columns[2].Visibility = System.Windows.Visibility.Hidden;
            dg_notes.Columns[3].Visibility = System.Windows.Visibility.Hidden;
            btn_addNote.Visibility = System.Windows.Visibility.Hidden;
            btn_addPhoneNumber.Visibility = System.Windows.Visibility.Hidden;
            tb_firstName.IsReadOnly = true;
            tb_secondName.IsReadOnly = true;
            btn_editRecordON.Visibility = System.Windows.Visibility.Visible;
            btn_editRecordOFF.Visibility = System.Windows.Visibility.Hidden;
            IsEditOn = false;
            lb_errors.Visibility = System.Windows.Visibility.Hidden;
        }

        #endregion

        #region Обработчики событий

        #region Обработчики событий. Форма.

        //При загрузке открывается выбор репозитория
        private void wnd_mWindow_Loaded(object sender, RoutedEventArgs e)
        {
            SelectRepository();
            if (IsAdmin)
            {
                EditOFF();
                btn_editRecordON.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        #endregion

        #region Обработчики событий. Левая панель.

        private void btn_view_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditOn)
            {
                MessageBox.Show("Завершите редактирование");
            }
            else
            {
                ViewSelectedRow();
            }
        }

        private void dg_adressView_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (IsEditOn)
            {
                MessageBox.Show("Завершите редактирование записи");
            }
            else
            {
                ViewSelectedRow();
            }
        }

        private void btn_search_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditOn)
            {
                MessageBox.Show("Завершите редактирование записи");
            }
            else
            {
                SearchWindow wnd_search = new SearchWindow();
                wnd_search.Owner = this;
                wnd_search.ShowDialog();
            }
        }

        private void btn_searchCancel_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditOn)
            {
                MessageBox.Show("Завершите редактирование записи");
            }
            else
            {
                //Отмена поиска
                resetQuery();
            }
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditOn)
            {
                MessageBox.Show("Завершите редактирование записи");
            }else
            {
                log.Info("Button Delete Click");
                if (MessageBox.Show(Properties.Resources.msg_deleteRecord,
                                    Properties.Resources.msg_deleteRecordHeder,
                                    MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    AddressRecord currentRecord = dg_adressView.SelectedItem as AddressRecord;
                    deleteRecord(currentRecord);

                    LoadData();
                }
            }
        }

        private void btn_createRecord_Click(object sender, RoutedEventArgs e)
        {
            if (IsEditOn)
            {
                MessageBox.Show("Завершите редактирование записи");
            }
            else
            {
                log.Info("Button CreateRecord Click");
                if (IsLoadedData)
                {
                    if (!IsRowEnitOn)
                    {
                        CustomizeForAdd();
                    }
                    else MessageBox.Show("Завершите редактирование открытой записи. Прежде чем добавить новую запись.");
                }
                else
                {
                    MessageBox.Show(Properties.Resources.exp_repositoryNotSelected);
                }
            }
        }
        #endregion

        #region Обработчики событий. Правая панель

        private void btn_changeUser_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button ChangeUser Click");
            AutorizationWindow wnd_passwordEnter = new AutorizationWindow();
            wnd_passwordEnter.Owner = this;
            wnd_passwordEnter.ShowDialog();
        }

        private void btn_chooseRepository_Click(object sender, RoutedEventArgs e)
        {
            SelectRepository();
        }

        //Обработчики событй над номерами телефонов
        private void btn_addPhoneNumber_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button addPhoneNumber Click");
            EditPhoneWindow editPhone_wnd = new EditPhoneWindow();
            editPhone_wnd.Owner = this;
            editPhone_wnd.ShowDialog();
        }

        private void btn_editPhoneNumber_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button editPhoneNumber Click");
            EditPhoneWindow editPhone_wnd = new EditPhoneWindow();
            editPhone_wnd.Owner = this;
            editPhone_wnd.showPhoneNumber((PhoneRecord)dg_phoneNubers.SelectedItem, ((PhoneRecord)dg_phoneNubers.SelectedItem).id);
            editPhone_wnd.Show();
        }

        private void btn_deletePhoneNumber_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button deletePhoneNumber Click");
            phoneRecords.Remove((PhoneRecord)dg_phoneNubers.SelectedItem);
            dg_phoneNubers.Items.Refresh();
        }

        //Обработчики событий над заметками
        private void btn_addNote_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button addNote Click");
            EditNoteWindows editNote_wnd = new EditNoteWindows();
            editNote_wnd.Owner = this;
            editNote_wnd.ShowDialog();
        }

        private void btn_editNote_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button editNote Click");
            EditNoteWindows editNote_wnd = new EditNoteWindows();
            editNote_wnd.Owner = this;
            editNote_wnd.showNote((Note)dg_notes.SelectedItem, ((Note)dg_notes.SelectedItem).id);
            editNote_wnd.Show();
        }

        private void btn_deleteNote_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button deleteNote Click");
            notes.Remove((Note)dg_notes.SelectedItem);
            dg_notes.Items.Refresh();
        }

        //Обработчики событий над записями 
        private void btn_editRecordON_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button editRecordON Click");
            addressRecordId = ((AddressRecord)dg_adressView.SelectedItem).id;
            EditON();
        }

        private void btn_editRecordOFF_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button editRecordOFF Click");
            if (phoneRecords.Count > 0)
            {
                try
                {
                    if (addressRecordId.Equals(Guid.Empty))
                    {

                        AddressRecord newAddressRecord = new AddressRecord(tb_firstName.Text,
                                                                           tb_secondName.Text,
                                                                           phoneRecords,
                                                                           notes);

                        addressRepository.Add(newAddressRecord);

                        EditOFF();
                    }
                    else
                    {

                        AddressRecord newAddressRecord = new AddressRecord(addressRecordId,
                                                                           tb_firstName.Text,
                                                                           tb_secondName.Text,
                                                                           phoneRecords,
                                                                           notes);
                        addressRepository.Update(newAddressRecord);
                        EditOFF();
                    }

                    LoadData();
                }
                catch (Exception ex) { lb_errors.Content = ex.Message; log.Error("Exeption: ", ex); }
            }
            else
            {
                lb_errors.Content = Properties.Resources.exp_notFoundPhones;
                log.Error("phones not found");
            }
        }

        private void btn_cancelEnditRecord_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion

        #endregion
    }
}
