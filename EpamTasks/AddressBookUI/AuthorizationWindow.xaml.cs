﻿//#define DEBUG_MODE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AddressBook;
using System.Configuration;
namespace AddressBookUI
{
    /// <summary>
    /// Interaction logic for AutorizationWindow.xaml
    /// </summary>
    public partial class AutorizationWindow : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AutorizationWindow()
        {
            InitializeComponent();
            log.Info("Initialized AutorizationWindow");
        }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button OK Click");
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                Configurations.AddressBookSectionGroup group = config.GetSectionGroup("addressBookGroup") as Configurations.AddressBookSectionGroup;
                Configurations.UsersSection usersSection = group.Users as Configurations.UsersSection;
                
                if (usersSection == null)
                    throw new Exception();

                bool find = false;

                foreach (Configurations.UserElement user in usersSection.Users)
                {
                    if (user.Login == tb_userLogin.Text)
                    {
                        find = true;
                        log.Info("Login finded");
                        if (userAuthentication(user.Password, tb_pswd.Password))
                        {
                            log.Info("User authenticated");
                            // Авторизация пользователя
                            if (Owner is MainWindow)
                            {
                                ((MainWindow)Owner).CustamizationProgram(user.Role);
                            }
                            else
                            {
                                MainWindow wnd_mainWindow = new MainWindow();
                                wnd_mainWindow.CustamizationProgram(user.Role);
                                wnd_mainWindow.Show();
                            }
                            Close();
                        }
                        else
                        {
                            log.Info("User not authenticated");
                            lb_errorPass.Visibility = Visibility.Visible;
                        }

                        break;
                    }
                }

                if(!find)
                    lb_errorPass.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Properties.Resources.exp_configLoadError);
                log.Fatal("Exeption:",ex);
                this.Close();
            }
        }

        /// <summary>
        /// Проверка соответствия имени пользователя и пороля
        /// </summary>
        /// <param name="password">Пароль пользователя. Из конфига.</param>
        /// <param name="enteredPassword">Введенный пользователем пароль. Из конфига.</param>
        /// <returns>true Если пользователь с таким именем и поролем существует</returns>
        private bool userAuthentication(string password, string enteredPassword)
        {
            bool rezult = false;
            if (password == enteredPassword)
                rezult = true;
            else
                rezult = false;
            return rezult;
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button Cancel Click");
            if (Owner is MainWindow)
            {
                Owner.Close();
            }
            Close();
        }

        private void wnd_autorization_Loaded(object sender, RoutedEventArgs e)
        {
#if DEBUG_MODE
            string UserName = "Igor";
            string UserPass = "111";

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                Configurations.AddressBookSectionGroup group = config.GetSectionGroup("addressBookGroup") as Configurations.AddressBookSectionGroup;
                Configurations.UsersSection usersSection = group.Users as Configurations.UsersSection;

                if (usersSection == null)
                    throw new Exception();

                bool find = false;

                foreach (Configurations.UserElement user in usersSection.Users)
                {
                    if (user.Login == UserName)
                    {
                        find = true;
                        log.Info("Login finded");
                        if (userAuthentication(user.Password, UserPass))
                        {
                            log.Info("User authenticated");
                            // Авторизация пользователя
                            if (Owner is MainWindow)
                            {
                                ((MainWindow)Owner).CustamizationProgram(user.Role);
                            }
                            else
                            {
                                MainWindow wnd_mainWindow = new MainWindow();
                                wnd_mainWindow.CustamizationProgram(user.Role);
                                wnd_mainWindow.Show();
                            }
                            Close();
                        }
                        else
                        {
                            log.Info("User not authenticated");
                            lb_errorPass.Visibility = Visibility.Visible;
                        }

                        break;
                    }
                }

                if (!find)
                    lb_errorPass.Visibility = Visibility.Visible;
           
#endif
        }
    }
}
