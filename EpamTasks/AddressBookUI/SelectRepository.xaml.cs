﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using DAL.RepositoryInfo;

namespace AddressBookUI
{
    /// <summary>
    /// Логика взаимодействия для SelectRepository.xaml
    /// </summary>
    public partial class SelectRepository : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SelectRepository()
        {
            InitializeComponent(); 
            log.Info("Initialized SelectRepository");
        }

        private void btn_select_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button Select Click");
            ((MainWindow)Owner).repositoryInfo = dg_repositories.SelectedItem as IRepositoryInfo;
            this.Close();
        }
        List<IRepositoryInfo> repositories = null;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadRepositoriesFromConfig();

            /*if (((MainWindow)Owner).IsAdmin)
                btn_addRepository.Visibility = System.Windows.Visibility.Visible;
            else
            {
                btn_addRepository.Visibility = System.Windows.Visibility.Hidden;
            }*/
        }

        private void loadRepositoriesFromConfig()
        {
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                Configurations.AddressBookSectionGroup group = config.GetSectionGroup("addressBookGroup") as Configurations.AddressBookSectionGroup;
                //Загрузка путей к схемам валидации, если они не обнавружены выбросить ошибку
                Configurations.ValidationSection validationSection = group.ValidationShemas as Configurations.ValidationSection;
                if (validationSection == null)
                    throw new Exception("Не обнаружены схемы валидации");

                Configurations.RepositoriesSection repositoriesSection = group.Repositories as Configurations.RepositoriesSection;

                if (repositoriesSection == null)
                    throw new Exception(Properties.Resources.exp_notFoundConfigSection);

                repositories = new List<IRepositoryInfo>();
                foreach (Configurations.RepositoryElement repository in repositoriesSection.Repositories)
                {
                    switch (repository.Type)
                    {
                        case "xml":
                            repositories.Add(GetXMLRepositoryInfo(repository, validationSection));
                            break;
                        case "binary":
                            repositories.Add(GetBinaryRepositoryInfo(repository));
                            break;
                    }
                }

                if (repositories.Count == 0)
                {
                    throw new Exception(Properties.Resources.exp_notFoundRepositoriesInConfig);
                }
                else
                {
                    dg_repositories.ItemsSource = repositories;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n" + Properties.Resources.exp_RepositoriesConfigError);
                log.Error("Exeption: ", ex);
                Close();
            }
        }

        private IRepositoryInfo GetXMLRepositoryInfo(Configurations.RepositoryElement repository, Configurations.ValidationSection validationSection)
        {
            //В зависимости от пипа валидации загрузка нужной схемы
            string shemaPath = null;
            System.Xml.ValidationType validationType;
            switch (repository.ValidationType)
            {
                case "dtd":
                    validationType = System.Xml.ValidationType.DTD;
                    shemaPath = validationSection.PathDTD;
                    break;
                case "xsd":
                    validationType = System.Xml.ValidationType.Schema;
                    shemaPath = validationSection.PathXSD;
                    break;
                default:
                    throw new Exception("Для репозитория задана неизвестная схема валидации");
            }

            return new XMLRepositoryInfo(repository.Name, repository.Path, validationType, shemaPath);
        }

        private IRepositoryInfo GetBinaryRepositoryInfo(Configurations.RepositoryElement repository)
        {
            return new BinaryRepositoryInfo(repository.Name, repository.Path);
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            log.Info("Button Cancel Click");
            this.Close();
        }

        private void btn_deleteRepository_Click(object sender, RoutedEventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            Configurations.AddressBookSectionGroup group = config.GetSectionGroup("addressBookGroup") as Configurations.AddressBookSectionGroup;
            //Загрузка путей к схемам валидации, если они не обнавружены выбросить ошибку
            Configurations.RepositoriesSection repositoriesSection = group.Repositories as Configurations.RepositoriesSection;

            if (repositoriesSection == null)
                throw new Exception(Properties.Resources.exp_notFoundConfigSection);

            if (MessageBox.Show("Подтвердите удаление репозитория", "Внимание", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                repositoriesSection.Repositories.Remove(((IRepositoryInfo)dg_repositories.SelectedItem).Name);
                config.Save();
                repositories.Remove((IRepositoryInfo)dg_repositories.SelectedItem);
                dg_repositories.Items.Refresh();
            }
        }

        private void btn_addRepository_Click(object sender, RoutedEventArgs e)
        {
            CreateRepository wnd_createRepository = new CreateRepository();
            wnd_createRepository.ShowDialog();
            //Если добавление репозитория было выполнено успешно 
            //Выполняем перезагрузку репозиториев из файла конфигурации

            loadRepositoriesFromConfig();
        }
    }
}
