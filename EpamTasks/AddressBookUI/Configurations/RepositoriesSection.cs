﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{
    public class RepositoriesSection: ConfigurationSection
    {
        /// <summary>
        /// Коллекция репозиториев
        /// </summary>
        [ConfigurationProperty("repositories")]
        public RepositoriesCollection Repositories
        {
            get { return (RepositoriesCollection)this["repositories"]; }
        }

        
    }
}
