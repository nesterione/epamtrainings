﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{
    public class AddressBookSectionGroup:ConfigurationSectionGroup
    {
        public AddressBookSectionGroup()
        {
        }

        #region Properties
        [ConfigurationProperty("repositoryConfiguration")]
        public RepositoriesSection Repositories
        {
            get { return (RepositoriesSection)base.Sections["repositoryConfiguration"]; }
        }

        [ConfigurationProperty("userConfiguration")]
        public UsersSection Users
        {
            get { return (UsersSection)base.Sections["userConfiguration"]; }
        }

        [ConfigurationProperty("validationConfiguration")]
        public ValidationSection ValidationShemas
        {
            get { return (ValidationSection)base.Sections["validationConfiguration"]; }
        }
        #endregion
    }
}
