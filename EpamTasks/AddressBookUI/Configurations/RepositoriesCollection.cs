﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{
    public class RepositoriesCollection : ConfigurationElementCollection
    {
        #region Overrides
        protected override ConfigurationElement CreateNewElement()
        {
            return new RepositoryElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RepositoryElement)element).Name;
        }
        #endregion

        #region methods
        public void Add(RepositoryElement item)
        {
            base.BaseAdd(item);
        }

        public void Remove(RepositoryElement item)
        {
            base.BaseRemove(item);
        }

        public void RemoveAt(int index)
        {
            base.BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            base.BaseRemove(name);
        }
        #endregion
    }
}
