﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{
    public class UserElement : ConfigurationElement
    {
        /// <summary>
        /// Логин пользователя
        /// </summary>
        [ConfigurationProperty("login", IsRequired = true)]
        public string Login
        {
            get { return (string)this["login"]; }
            set { this["login"] = value; }
        }

        /// <summary>
        /// Роль пользователя
        /// </summary>
        [ConfigurationProperty("role", IsRequired = true)]
        public string Role
        {
            get { return (string)this["role"]; }
            set { this["role"] = value; }
        }

        /// <summary>
        /// Пароль
        /// </summary>
        [ConfigurationProperty("password", IsRequired = true)]
        public string Password
        {
            get { return (string)this["password"]; }
            set { this["password"] = value; }
        }
    }
}
