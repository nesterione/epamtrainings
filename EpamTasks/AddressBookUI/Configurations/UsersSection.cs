﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{ 
    public class UsersSection:ConfigurationSection
    {
        /// <summary>
        /// Коллекция пользователей
        /// </summary>
        [ConfigurationProperty("users")]
        public UsersCollection Users
        {
            get { return (UsersCollection)this["users"]; }
        }
    }
}
