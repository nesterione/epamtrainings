﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{
    public class ValidationSection : ConfigurationSection
    {
        /// <summary>
        /// Путь к DTD схеме
        /// </summary>
        [ConfigurationProperty("DTD", IsRequired = true)]
        public string PathDTD
        {
            get { return (string)this["DTD"]; }
            set { this["DTD"] = value; }
        }

        /// <summary>
        /// Путь к XSD схеме
        /// </summary>
        [ConfigurationProperty("XSD", IsRequired = true)]
        public string PathXSD
        {
            get { return (string)this["XSD"]; }
            set { this["XSD"] = value; }
        }
    }
}
