﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace AddressBookUI.Configurations
{
    public class RepositoryElement : ConfigurationElement
    {
        public RepositoryElement()
        {
        }
        public RepositoryElement(string Name, string Path, string Type)
        {
            this.Name = Name;
            this.Path = Path;
            this.Type = Type;
        }

        public RepositoryElement(string Name, string Path, string Type, string ValidationType)
        {
            this.Name = Name;
            this.Path = Path;
            this.Type = Type;
            this.ValidationType = ValidationType;
        }

        /// <summary>
        /// Имя репозитория
        /// </summary>
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        /// <summary>
        /// Путь репозитория
        /// </summary>
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path
        {
            get { return (string)this["path"]; }
            set { this["path"] = value; }
        }

        /// <summary>
        /// Тип репозитория
        /// </summary>
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type
        {
            get { return (string)this["type"]; }
            set { this["type"] = value; }
        }

        /// <summary>
        /// Тип валидации
        /// </summary>
        [ConfigurationProperty("validationType", DefaultValue = "xsd")]
        public string ValidationType
        {
            get { return (string)this["validationType"]; }
            set { this["validationType"] = value; }
        }
    }
}
