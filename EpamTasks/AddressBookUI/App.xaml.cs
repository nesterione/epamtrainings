﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Globalization;
using System.Windows.Markup;
using AddressBookUI.Models;
using AddressBookUI.ViewModels;
using AddressBookUI.Views;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace AddressBookUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            List<Language> languages = new List<Language>()
            {
                new Language("English","en-US"),
                new Language("Русский","ru-RU"),
                new Language("Deutch","de-DE")
            };

            AddressBookUI.Views.SelectLanguage view = new AddressBookUI.Views.SelectLanguage(); // создали View
            SelectLanguageViewModel viewModel = new ViewModels.SelectLanguageViewModel(languages); // Создали ViewModel
            view.DataContext = viewModel; // положили ViewModel во View в качестве DataContext
            view.Show();
        }
    }


}
