﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;
using System.Linq.Expressions;

namespace TestLinqToXPath
{
    class Program
    {
        static void Main(string[] args)
        {
            DAL.IRepository<AddressRecord, Guid> repository = new DAL.RepositoryXML.AddressRepositoryXML("D:\\TEMP1\\XMLRepository2.xml", System.Xml.ValidationType.Schema, "d:\\TEMP1\\XMLRepository.xsd");
            
            var dd = repository.Get();

            //x => x.secondName.Equals("Petrov")
            //var records = (from record in dd
            //                              where record.secondName == "Nesterenya"||record.firstName=="Peter"
            //               select record.firstName).ToList<string>();

            //var records = (from record in dd
            //               where record.firstName == "Igor" && record.secondName == "Nesfterenya"
            //               select record).First();

            //var records = (from record in dd
            //               where record.firstName == "Igor" && record.secondName == "Nesterenya"
            //               select record).First().firstName;


            //var records = from record in dd
            //               where (record.notes.Where(note => note.tags.Where(tag => tag.Text == "tag1").Count() >0).Count()>0)
            //               select record;


            //AddressRecord
            //IdRecord => id
            //FirstName => firstName
            //SecondName => secondName
            //LastChangeTime => lastChTime
            //ns1:PhoneRecords => phoneNumbers
            ////ns1:PhoneRecord =>
            //////ns1:IdPhoneRecord => id
            //////ns1:Name => name
            //////ns1:PhoneNumber => phoneNumber
            ////////ns1:IdPhoneNumber => id
            ////////ns1:CountryCode => countryCode
            ////////ns1:Phone => phone
            //ns1:Notes => notes
            ////IdNote => id
            ////Text => text
            ////Tags => tags
            //////Tag => Text

            //tagsString - запретить
            //2011-10-26T21:32:52

            //var records = (from record in dd
            //              where record.firstName == "Igor"
            //              select record).ToList();
            //2011-10-26T21:32:52

            //repository.Get().Where((x) => x.secondName == "Nesterenya").ToList<AddressRecord>();

            //var t01 = dd.Where(record => record.firstName == "Igor").ToList();
            //var t02 = dd.Where(record => record.secondName == "Nesterenya").ToList();
            //var t03 = dd.Where(record => record.id == (new Guid("05b3f6cb-65e9-431e-a36c-2d2cdfb5700d"))).ToList();
            //var t04 = dd.Where(record => record.lastChTime == df).ToList();
            //var t041 = dd.Where(record => record.lastChTime == df).ToList();
            //var t042 = dd.Where(record => record.lastChTime == df).ToList();
            //var t05 = dd.Where(record => record.notes.Where( nt => nt.id==(new Guid("8F9619FF-8B86-FFFF-0001-00CF4FC968B3"))).Count()>0).ToList();
            //var t06 = dd.Where(record => record.notes.Where(nt => nt.text == "Notification").Count() > 0).ToList();
            //var t07 = dd.Where(record => record.notes.Where(nt => nt.tags.Where(tg => tg.text=="tag1").Count()>0).Count()>0).ToList();
            //var t08 = dd.Where(record => record.phoneNumbers.Where(pn => pn.id == (new Guid("6F9619FF-8B86-D011-B42D-10CF4FC964B3"))).Count()>0).ToList();
            //var t09 = dd.Where(record => record.phoneNumbers.Where(pn => pn.name == "Home").Count() > 0).ToList();
            //var t10 = dd.Where(record => record.phoneNumbers.Where(pn => pn.phoneNumber.id == (new Guid("6F9619FF-8B87-FFFF-FFFF-00CF4FC964AC"))).Count() > 0).ToList();
            //var t11 = dd.Where(record => record.phoneNumbers.Where(pn => pn.phoneNumber.phone == "34534534").Count() > 0).ToList();
            //var t12 = dd.Where(record => record.phoneNumbers.Where(pn => pn.phoneNumber.id == "34").Count() > 0).ToList();

            var r1 = (from record in dd
                           where record.firstName == "Igor"
                           select record).First();

            var r2 = (from record in dd
                           where record.firstName == "Igor"
                           select record).Last();

       

        }
    }
}
