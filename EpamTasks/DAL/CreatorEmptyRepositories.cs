﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DAL
{
    public static class CreatorEmptyRepositories
    {
        public static bool CreteBinaryRepository(string newRepositoryLocaion)
        {
            bool success = false;
            try
            {
                List<AddressBook.AddressRecord> addressRecords = new List<AddressBook.AddressRecord>();
                FileStream fileStream = new FileStream(newRepositoryLocaion, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fileStream, addressRecords);
                fileStream.Close();
                success = true;
            }
            catch {success = false;};
            return success;
        }

        public static bool CreateXMLRepository(string newRepositoryLocation)
        {
            bool success = false;
            string Heder = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<AddressBook xmlns:ns1=\"namespace1\" xmlns:ns2=\"namespace2\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema-instance\">\n";
            string Footer = "</AddressBook>";
            try
            {
                StreamWriter sw = new StreamWriter(new FileStream(newRepositoryLocation, FileMode.Create, FileAccess.Write, FileShare.ReadWrite));
                sw.Write(Heder + Footer);
                sw.Close();
                success=true;
            }
            catch {success = false;}
            return success;
        }
    }
}
