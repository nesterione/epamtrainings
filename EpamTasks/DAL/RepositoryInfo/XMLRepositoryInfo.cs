﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.RepositoryInfo
{
    public class XMLRepositoryInfo : IRepositoryInfo
    {
        private string name;
        private string path;
        private string schemaPath;
        private System.Xml.ValidationType validationType;

        public XMLRepositoryInfo(string name, string path, System.Xml.ValidationType validationType, string schemaPath)
        {
            this.name = name;
            this.path = path;
            this.schemaPath = schemaPath;
            this.validationType = validationType;
        }

        public IRepository<AddressBook.AddressRecord, Guid> CreateRepository()
        {
            return new RepositoryXML.AddressRepositoryXML(path, validationType, schemaPath);
        }

        public string Info
        {
            get { return name + "(" + "XML - " + path + ")"; }
        }


        public string Name
        {
            get { return name; }
        }
    }
}
