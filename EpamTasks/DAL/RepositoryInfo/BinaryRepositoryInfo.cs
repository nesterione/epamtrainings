﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.RepositoryInfo
{
    public class BinaryRepositoryInfo:IRepositoryInfo
    {
        private string path;
        private string name;

        public BinaryRepositoryInfo(string name, string path)
        {
            this.name = name;
            this.path = path;
        }

        public IRepository<AddressBook.AddressRecord, Guid> CreateRepository()
        {
            return new RepositoryMemory.AddressRepositoryMemory(path);
        }

        public string Info
        {
            get { return name + "(" + "Binary - " + path + ")"; }
        }


        public string Name
        {
            get { return name; }
        }
    }
}
