﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.RepositoryInfo
{
    public interface IRepositoryInfo
    {
         IRepository<AddressBook.AddressRecord, Guid> CreateRepository();
         string Info { get; }
         string Name { get; }
    }
}
