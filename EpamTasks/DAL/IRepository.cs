﻿using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public interface IRepository<TEntity, in TKey> where TEntity: class
    {
        List<TEntity> GetAll();
        IQueryable<TEntity> Get();
        void Update(TEntity entity);
        void Add(TEntity entity);
        void Delete(TEntity entity);
    }
}
