﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;
using System.Linq.Expressions;

namespace DAL.Search
{
    public class SimpleSearch:ISearch
    {
        string query;

        public SimpleSearch(string query)
        {
            this.query = query;
        }

        private Expression<Func<AddressRecord, bool>> SearchExpression
        {
            get
            {
                return (record) =>
                        record.firstName == query ||
                        record.secondName == query ||
                        record.notes.Where(
                                               note => note.text == query ||
                                               note.tags.Where(
                                                                tag => tag.text == query
                                                              ).Count() > 0
                                          ).Count() > 0;
            }
        }

        public List<AddressRecord> Search(IQueryable<AddressRecord> repository)
        {
            return repository.Where(SearchExpression).ToList<AddressRecord>();
        }
    }
}
