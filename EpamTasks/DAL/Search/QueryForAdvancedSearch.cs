﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Search
{
    public class QueryForAdvancedSearch
    {
        public string firstName {get; private set;}
        public string secondName { get; private set; }
        public string textNote { get; private set; }
        public string tag { get; private set; }
        public string countryCode { get; private set; }
        public string phoneNumber { get; private set; }
        public string namePhone { get; private set; }
        public DateTime? startDate { get; private set; }
        public DateTime? endDate { get; private set; }

        public QueryForAdvancedSearch(  string firstName,
                                        string secondName,
                                        string textNote,
                                        string tag,
                                        string countryCode,
                                        string phoneNumber,
                                        string namePhone,
                                        DateTime? startDate,
                                        DateTime? endDate)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.textNote = textNote;
            this.tag = tag;
            this.countryCode = countryCode;
            this.phoneNumber = phoneNumber;
            this.namePhone = namePhone;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
}
