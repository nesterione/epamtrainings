﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;
using System.Linq.Expressions;

namespace DAL.Search
{
    public class AdvancedSearch:ISearch
    {
        QueryForAdvancedSearch query;

        public AdvancedSearch(QueryForAdvancedSearch query)
        {
            this.query = query;
        }

        private Expression<Func<AddressRecord, bool>> formExprFirstName(string firstName)
        {
            return (record) => record.firstName.Contains(firstName);
        }

        private Expression<Func<AddressRecord, bool>> formExprSecondName(string secondName)
        {
            
            return (record) => record.secondName.Contains(secondName);
        }

        private Expression<Func<AddressRecord, bool>> formExprTextNote(string textNote)
        {
            return (record) => record.notes.Where(note => note.text.Contains(textNote)).Count()>0;
        }

        private Expression<Func<AddressRecord, bool>> formExprTag(string tag)
        {
            return (record) => record.notes.Where(note => note.tags.Where(tg => tg.text.Contains(tag)).Count()>0).Count() > 0;
        }

        private Expression<Func<AddressRecord, bool>> formExprCountryCode(string countryCode)
        {
            return (record) => record.phoneNumbers.Where(phone=>phone.phoneNumber.countryCode.ToString().Contains(countryCode)).Count()>0;
        }

        private Expression<Func<AddressRecord, bool>> formExprPhoneNumber(string phoneNumber)
        {
            throw new Exception("sdf");
        }

        private Expression<Func<AddressRecord, bool>> formExprNamePhone(string nemePhone)
        {
            throw new Exception("sdf");
        }

        private Expression<Func<AddressRecord, bool>> formExprDate(DateTime startDate, DateTime endDate)
        {
            throw new Exception("sdf");
        }

        private Expression<Func<AddressRecord, bool>> formSearchExpression()
        {
            List<Expression<Func<AddressRecord, bool>>> exprs = new List<Expression<Func<AddressRecord, bool>>>();
            //Для поиска по имени
            if (query.firstName != null)
                exprs.Add(formExprFirstName(query.firstName));
            //Для поиска по фамилии
            if (query.secondName != null)
                exprs.Add(formExprSecondName(query.secondName));
            //Для поиска по тексту заметки
            if (query.textNote != null)
                exprs.Add(formExprTextNote(query.textNote));
            //Для поиска по тегу
            if (query.tag != null)
                exprs.Add(formExprTag(query.tag));
            //Для поиска по коду страны
            if (query.countryCode != null)
                exprs.Add(formExprCountryCode(query.countryCode));
            //Для поиска по номеру телефона
            if (query.phoneNumber != null)
                exprs.Add(formExprPhoneNumber(query.phoneNumber));
            //Для поиска по названию номера телефона
            if (query.namePhone != null)
                exprs.Add(formExprNamePhone(query.namePhone));
            //Для поиска по дате
            if ((query.startDate != null) && (query.endDate != null))
                exprs.Add(formExprDate(query.startDate.Value, query.endDate.Value));

            Expression<Func<AddressRecord, bool>> rezult = null;
            if(exprs.Count == 0)
                    throw new Exception("Не заданы условия поиска");
             if(exprs.Count == 1)
             {
                 rezult = exprs[0];
             }
            else
             {
                 //Цикл должен нацинаться с 1, потому что первый элемент
                 //уже использован, и не должен быть повторно быть использован в цикле.
                 rezult = exprs[0];
                 for (int i = 1; i < exprs.Count; i++)
                 {
                     BinaryExpression d = Expression<Func<AddressRecord, bool>>.AndAlso(rezult.Body, exprs[i].Body);
                     ParameterExpression abParam = Expression.Parameter(typeof(AddressRecord));
                     rezult =  Expression.Lambda<Func<AddressRecord, bool>>(d,new ParameterExpression[] { abParam });
                 }
             }

            return rezult;
        }

        public List<AddressRecord> Search( IQueryable<AddressRecord> repository)
        {
            return repository.Where(formSearchExpression()).ToList<AddressRecord>();
        }
    }
}
