﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;

namespace DAL.Search
{
    public interface ISearch
    {
        List<AddressRecord> Search( IQueryable<AddressRecord> repository);
    }
}
