﻿using AddressBook;
using DAL.RepositoryXML.LinqToXPath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;

namespace DAL.RepositoryXML
{
    public class AddressRepositoryXML : IRepository<AddressRecord, Guid>
    {
        #region Members
        XmlDocument addressBookStorage = null;
        string XMLDocumentPath = null;
        string addressRecordByIdXPath = "AddressRecord[ translate(IdRecord,'ABCDEF','abcdef')='{0}']";
        string RootElement = "AddressBook";
        #endregion

        public AddressRepositoryXML(string XMLDocumentPath, ValidationType validationType, string SchemaPath)
        {
            try
            {
                if (!(System.IO.File.Exists(XMLDocumentPath) && System.IO.File.Exists(SchemaPath)))
                    throw new System.IO.FileNotFoundException();
                XmlReaderSettings settings = null;
                this.XMLDocumentPath = XMLDocumentPath;
                addressBookStorage = new XmlDocument();

                switch (validationType)
                {
                    case ValidationType.Schema:
                        
                        XmlSchemaSet schema = new XmlSchemaSet();
                        schema.Add(null, SchemaPath);
                        settings = new XmlReaderSettings();
                        settings.DtdProcessing = DtdProcessing.Ignore;
                        settings.ValidationType = ValidationType.Schema;
                        settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);
                        settings.Schemas.Add(schema);
                        settings.NameTable = new NameTable();

                        using (XmlReader reader = XmlReader.Create(XMLDocumentPath, settings))
                        {
                            addressBookStorage.Load(reader);
                        }
                        break;
                    case ValidationType.DTD:

                        settings = new XmlReaderSettings();
                        settings.DtdProcessing = DtdProcessing.Parse;
                        settings.ValidationType = ValidationType.DTD;
                        settings.ValidationEventHandler += new ValidationEventHandler(ValidationEventHandler);
                        settings.NameTable = new NameTable();

                        XmlParserContext context = new XmlParserContext(settings.NameTable,
                                                                        new XmlNamespaceManager(settings.NameTable),
                                                                        RootElement,
                                                                        "",
                                                                        SchemaPath,
                                                                        "",
                                                                        "",
                                                                        "en",
                                                                        XmlSpace.Default);

                        using (XmlReader reader = XmlReader.Create(XMLDocumentPath, settings, context))
                        {
                            try
                            {
                                addressBookStorage.Load(reader);
                            }
                            catch 
                            {
                                throw;
                            }
                        }
                        break;
                }


            }
            catch { throw; }
        }

        void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    throw new XmlSchemaValidationException("Error: " + e.Message);
                case XmlSeverityType.Warning:
                    throw new XmlSchemaValidationException("Warning: " + e.Message);
            }
        }

        #region Реализация методов интерфейса IRepository

        public List<AddressRecord> GetAll()
        {
            List<AddressRecord> o_addressRecords = new List<AddressRecord>();
            XmlNodeList addressRecords = addressBookStorage.GetElementsByTagName("AddressRecord");
            return AddressRepositoryXMLContext.ConvertNodesToList(addressRecords);
        }

        public void Update(AddressRecord entity)
        {
            XmlNode root = addressBookStorage.DocumentElement;
            XmlNode node = root.SelectSingleNode(String.Format(addressRecordByIdXPath, entity.id.ToString()));
            if (node == null)
                Add(entity);
            XmlNode outer = node.ParentNode;
            XmlNode newNode = createAddressRecord(entity);

            outer.ReplaceChild(newNode, node);
            addressBookStorage.Save(XMLDocumentPath);
        }

        public void Add(AddressRecord entity)
        {
            XmlNode addressRecordNode = createAddressRecord(entity);
            addressBookStorage.DocumentElement.AppendChild(addressRecordNode);
            addressBookStorage.Save(XMLDocumentPath);
        }

        public void Delete(AddressRecord entity)
        {
            XmlNode root = addressBookStorage.DocumentElement;
            XmlNode node = root.SelectSingleNode(String.Format(addressRecordByIdXPath, entity.id.ToString()));
            if (node != null)
            {
                XmlNode outer = node.ParentNode;
                outer.RemoveChild(node);
                addressBookStorage.Save(XMLDocumentPath);
            }
        }

        public IQueryable<AddressRecord> Get()
        {
            return new QueryableXMLData<AddressRecord>(addressBookStorage);
        }

        #endregion

        #region Вспомогательные методы

        private XmlNode createAddressRecord(AddressRecord entity)
        {
            XmlElement addressRecordNode = addressBookStorage.CreateElement("AddressRecord");

            XmlElement idRecordNode = addressBookStorage.CreateElement("IdRecord");
            idRecordNode.InnerText = entity.id.ToString();
            addressRecordNode.AppendChild(idRecordNode);

            XmlElement firstNameNode = addressBookStorage.CreateElement("FirstName");
            firstNameNode.InnerText = entity.firstName.ToString();
            addressRecordNode.AppendChild(firstNameNode);

            XmlElement secondNameNode = addressBookStorage.CreateElement("SecondName");
            secondNameNode.InnerText = entity.secondName.ToString();
            addressRecordNode.AppendChild(secondNameNode);

            XmlElement lastChangeTime = addressBookStorage.CreateElement("LastChangeTime");
            lastChangeTime.InnerText = entity.lastChTime.ToString("s");
            addressRecordNode.AppendChild(lastChangeTime);

            addressRecordNode.AppendChild(createPhoneRecordsNode(entity.phoneNumbers));
            addressRecordNode.AppendChild(createNotesNode(entity.notes));

            return addressRecordNode;
        }

        private XmlNode createPhoneRecordsNode(List<PhoneRecord> phoneRecords)
        {
            XmlElement phoneRecordsNode = addressBookStorage.CreateElement("PhoneRecords", "namespace1");
            phoneRecordsNode.Prefix = "ns1";

            foreach (PhoneRecord phoneRecord in phoneRecords)
            {
                XmlElement phoneRecordNode = addressBookStorage.CreateElement("PhoneRecord", "namespace1");
                phoneRecordNode.Prefix = "ns1";

                XmlElement idPhoneRecordNode = addressBookStorage.CreateElement("IdPhoneRecord", "namespace1");
                idPhoneRecordNode.Prefix = "ns1";
                idPhoneRecordNode.InnerText = phoneRecord.id.ToString();
                phoneRecordNode.AppendChild(idPhoneRecordNode);

                XmlElement nameNode = addressBookStorage.CreateElement("Name", "namespace1");
                nameNode.Prefix = "ns1";
                nameNode.InnerText = phoneRecord.name.ToString();
                phoneRecordNode.AppendChild(nameNode);

                phoneRecordNode.AppendChild(createPhoneNode(phoneRecord.phoneNumber));

                phoneRecordsNode.AppendChild(phoneRecordNode);
            }

            return phoneRecordsNode;
        }

        private XmlNode createPhoneNode(PhoneNumber phone)
        {
            XmlElement phoneNumberNode = addressBookStorage.CreateElement("PhoneNumber", "namespace1");
            phoneNumberNode.Prefix = "ns1";

            XmlElement idPhoneNumberNode = addressBookStorage.CreateElement("IdPhoneNumber", "namespace1");
            idPhoneNumberNode.Prefix = "ns1";
            idPhoneNumberNode.InnerText = phone.id.ToString();
            phoneNumberNode.AppendChild(idPhoneNumberNode);

            XmlElement countryCodeNode = addressBookStorage.CreateElement("CountryCode", "namespace1");
            countryCodeNode.Prefix = "ns1";
            countryCodeNode.InnerText = phone.countryCode.ToString();
            phoneNumberNode.AppendChild(countryCodeNode);

            XmlElement phoneNode = addressBookStorage.CreateElement("Phone", "namespace1");
            phoneNode.Prefix = "ns1";
            phoneNode.InnerText = phone.phone.ToString();
            phoneNumberNode.AppendChild(phoneNode);

            return phoneNumberNode;
        }

        private XmlNode createNotesNode(List<Note> notes)
        {
            XmlElement notesNode = addressBookStorage.CreateElement("Notes", "namespace2");
            notesNode.Prefix = "ns2";
            foreach (Note note in notes)
            {
                XmlElement noteNode = addressBookStorage.CreateElement("Note", "namespace2");
                noteNode.Prefix = "ns2";

                XmlElement idNoteNode = addressBookStorage.CreateElement("IdNote", "namespace2");
                idNoteNode.Prefix = "ns2";
                idNoteNode.InnerText = note.id.ToString();
                noteNode.AppendChild(idNoteNode);

                XmlElement textNode = addressBookStorage.CreateElement("Text", "namespace2");
                textNode.Prefix = "ns2";
                textNode.InnerText = note.text.ToString();
                noteNode.AppendChild(textNode);
                noteNode.AppendChild(createTagsNode(note.tags));
                notesNode.AppendChild(noteNode);
            }

            return notesNode;
        }

        private XmlNode createTagsNode(List<Tag> tags)
        {
            XmlElement tagsNode = addressBookStorage.CreateElement("Tags", "namespace2");
            tagsNode.Prefix = "ns2";

            foreach (Tag tag in tags)
            {
                XmlElement tagNode = addressBookStorage.CreateElement("Tag", "namespace2");
                tagNode.Prefix = "ns2";
                tagNode.InnerText = tag.ToString();
                tagsNode.AppendChild(tagNode);
            }
            return tagsNode;
        }

        #endregion
    }
}
