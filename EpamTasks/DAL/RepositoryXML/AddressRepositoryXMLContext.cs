﻿using AddressBook;
using System;
using System.Collections.Generic;
using System.Xml;

namespace DAL.RepositoryXML
{
    class AddressRepositoryXMLContext
    {
        internal static AddressRecord ConvertNodeToAddressRecord(XmlNode addressRecord)
        {
            Guid idRecord = Guid.Parse(addressRecord["IdRecord"].InnerText);
            string firstName = addressRecord["FirstName"].InnerText;
            string secondName = addressRecord["SecondName"].InnerText;
            DateTime lastChangeTime = DateTime.Parse(addressRecord["LastChangeTime"].InnerText);

            //считывание номеров телефонов
            List<PhoneRecord> o_phoneRecords = new List<PhoneRecord>();
            XmlNodeList phoneRecords = addressRecord["ns1:PhoneRecords"].GetElementsByTagName("ns1:PhoneRecord");
            foreach (XmlNode phoneRecord in phoneRecords)
            {
                // Считывание записи номера телефона
                PhoneRecord o_phoneRecord = null;
                {
                    Guid idPhoneRecord = Guid.Parse(phoneRecord["ns1:IdPhoneRecord"].InnerText);
                    string Name = phoneRecord["ns1:Name"].InnerText;

                    //Считывание Номера телефона
                    PhoneNumber o_phoneNumber = null;
                    {
                        Guid idPhoneNumber = Guid.Parse(phoneRecord["ns1:PhoneNumber"]["ns1:IdPhoneNumber"].InnerText);
                        int countryCode = int.Parse(phoneRecord["ns1:PhoneNumber"]["ns1:CountryCode"].InnerText);
                        long phone = long.Parse(phoneRecord["ns1:PhoneNumber"]["ns1:Phone"].InnerText);

                        o_phoneNumber = new PhoneNumber(idPhoneNumber, countryCode, phone);
                    }//Конец, считывание номера телефона

                    o_phoneRecord = new PhoneRecord(idPhoneRecord, Name, o_phoneNumber);
                }// Конец, Считывание записи номера телефона

                o_phoneRecords.Add(o_phoneRecord);
            }//Конец, считывание номеров телефонов

            //Считывание Заметок
            List<Note> o_notes = new List<Note>();
            XmlNodeList notes = addressRecord["ns2:Notes"].GetElementsByTagName("ns2:Note");
            foreach (XmlNode note in notes)
            {
                Guid idNote = Guid.Parse(note["ns2:IdNote"].InnerText);
                string textNote = note["ns2:Text"].InnerText;

                //Считывание тегов
                List<Tag> o_tags = new List<Tag>();
                foreach (XmlNode tag in note["ns2:Tags"].GetElementsByTagName("ns2:Tag"))
                {
                    o_tags.Add(new Tag(tag.InnerText));
                }

                o_notes.Add(new Note(textNote, o_tags.ToArray()));
            }//Конец, считывание заметок

            return new AddressRecord(idRecord, firstName, secondName, lastChangeTime, o_phoneRecords, o_notes);
            
        }

        internal static List<AddressRecord> ConvertNodesToList(XmlNodeList addressRecords)
        {
            List<AddressRecord> o_addressRecords = new List<AddressRecord>();

            foreach (XmlNode addressRecord in addressRecords)
            {
                o_addressRecords.Add(ConvertNodeToAddressRecord(addressRecord));    
            }
            return o_addressRecords;
        }
    }
}
