﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;

namespace DAL.RepositoryXML.LinqToXPath
{
    public class QueryableXMLData<T> : IQueryable<T>, IQueryable, IEnumerable<T>, IEnumerable, IOrderedQueryable<T>, IOrderedQueryable
    {
        #region Properties
       
        public IQueryProvider Provider { get; private set; }
        public Expression Expression { get; private set; }
        Type IQueryable.ElementType  { get { return typeof(T); }}

        #endregion

        public QueryableXMLData(XmlDocument document)
        {
            this.Provider = new XMLQueryProvider(document);
            this.Expression= Expression.Constant(this);
        }

        public QueryableXMLData(XMLQueryProvider provider, Expression expression)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            if (!typeof(IQueryable<T>).IsAssignableFrom(expression.Type))
            {
                throw new ArgumentOutOfRangeException("expression");
            }
            
            this.Provider = provider;
            this.Expression = expression;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)this.Provider.Execute(this.Expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this.Provider.Execute(this.Expression)).GetEnumerator();
        }
    }
}
