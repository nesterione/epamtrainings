﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DAL.RepositoryXML.LinqToXPath
{
    internal class QueryTranslator : ExpressionVisitor
    {
        StringBuilder sb;

        internal QueryTranslator()
        {
        }

        internal string Translate(Expression expression)
        {
            this.sb = new StringBuilder();
            this.Visit(expression);
            return this.sb.ToString();
        }

        protected override Expression VisitMethodCall(MethodCallExpression m)
        {
            string method = m.Method.Name;

            if (method == "Where")
            {
                string ap = "";
                switch (m.Type.GetGenericArguments()[0].FullName.Split('.').Last())
                {
                    case "AddressRecord":
                        ap = "//AddressBook//AddressRecord";
                        break;
                    case "Note":
                        ap = ".//ns2:Notes";
                        break;
                    case "Tag":
                        ap = ".//ns2:Tags";
                        break;
                }

                sb.Append(ap);
                sb.Append("[");
                this.Visit(m.Arguments[0]);
                LambdaExpression lambda = (LambdaExpression)StripQuotes(m.Arguments[1]);
                lambda = (LambdaExpression)Evaluator.PartialEval(lambda);
                this.Visit(lambda.Body);
                sb.Append("]");
                return m;
            }

            if (method == "Count")
            {
                sb.Append("count(");
                this.Visit(m.Arguments[0]);
                sb.Append(")");
                return m;
            }

            if (method == "Contains")
            {
                sb.Append("contains(");
                sb.Append(mapMemberName(((MemberExpression)m.Object).Member.Name, ((MemberExpression)m.Object).Expression.Type.FullName.Split('.').Last()));
                sb.Append(",");
                this.Visit(m.Arguments[0]);
                sb.Append(")");
                return m;
            }

            if (method == "First" || method == "FirstOrDefault")
            {
                sb.Append("(");
                this.Visit(m.Arguments[0]);
                sb.Append(")[1]");
                return m;
            }

            if (method == "Last" || method == "LastOrDefault")
            {
                sb.Append("(");
                this.Visit(m.Arguments[0]);
                sb.Append(")[last()]");
                return m;
            }

            throw new NotSupportedException(string.Format("The method '{0}' is not supported", m.Method.Name));
        }

        #region Visitors

        protected override Expression VisitUnary(UnaryExpression u)
        {
            switch (u.NodeType)
            {
                case ExpressionType.Not:
                    sb.Append(" ! ");
                    this.Visit(u.Operand);
                    break;
                default:
                    throw new NotSupportedException(string.Format("The unary operator '{0}' is not supported", u.NodeType));
            }
            return u;
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            this.Visit(b.Left);
            sb.Append(binaryOperatorTranlsate(b.NodeType));
            this.Visit(b.Right);
            return b;
        }

        protected override Expression VisitConstant(ConstantExpression c)
        {
            IQueryable q = c.Value as IQueryable;
            if (q != null)
            {
                //sb.Append(".//" + q.ElementType.Name);
            }
            else if (c.Value == null)
            {
                sb.Append("''");
            }
            else
            {
                switch (Type.GetTypeCode(c.Value.GetType()))
                {
                    case TypeCode.Boolean:
                        //sb.Append(((bool)c.Value) ? 1 : 0);
                        break;
                    case TypeCode.String:
                        sb.Append("'");
                        sb.Append(c.Value);
                        sb.Append("'");
                        break;
                    case TypeCode.Object:
                        sb.Append("'");
                        sb.Append(c.Value.ToString());
                        sb.Append("'");
                        break;
                    default:
                        sb.Append(c.Value);
                        break;
                }
            }
            return c;
        }

        protected override MemberAssignment VisitMemberAssignment(MemberAssignment m)
        {
            if (m.Expression != null && m.Expression.NodeType == ExpressionType.Parameter)
            {
                sb.Append(m.Member.Name);
                return m;
            }
            throw new NotSupportedException(string.Format("The member '{0}' is not supported", m.Member.Name));
        }

        protected override Expression VisitMember(MemberExpression m)
        {
            string append = "";
            string callType = m.Expression.Type.FullName.Split('.').Last();

            if (m.Expression != null && m.Expression.NodeType == ExpressionType.Parameter)
            {
                append = mapMemberName(m.Member.Name, callType);
            }
            sb.Append(append);

            return m;
        }

        #endregion

        #region Methods

        private string binaryOperatorTranlsate(ExpressionType type)
        {
            string rezult = "";
            switch (type)
            {
                case ExpressionType.And:
                    rezult = " and ";
                    break;
                case ExpressionType.AndAlso:
                    rezult = " and ";
                    break;
                case ExpressionType.Or:
                    rezult = " or";
                    break;
                case ExpressionType.Equal:
                    rezult = " = ";
                    break;
                case ExpressionType.NotEqual:
                    rezult = " != ";
                    break;
                case ExpressionType.LessThan:
                    rezult = " < ";
                    break;
                case ExpressionType.LessThanOrEqual:
                    rezult = " <= ";
                    break;
                case ExpressionType.GreaterThan:
                    rezult = " > ";
                    break;
                case ExpressionType.GreaterThanOrEqual:
                    rezult = " >= ";
                    break;
                case ExpressionType.OrElse:
                    rezult = " or ";
                    break;
                default:
                    throw new NotSupportedException(string.Format("The binary operator '{0}' is not supported", type));
            }

            return rezult;
        }

        // Метод преабразуте название, в соответствующее ему название 
        // в XPath, в данноме методе не был использован swith, так как 
        // для некоторых названий нужны дополнительные проверки в условии
        // поэтому сдесь сдела метод с моножественным выходом.
        private string mapMemberName(string name, string callType)
        {
            // - запретить
            if (name == "firstName")
                return "FirstName";
            if (name == "secondName")
                return "SecondName";
            //LastChangeTime => lastChTime
            if (name == "countryCode")
                return "ns1:CountryCode";
            if (name == "phone")
                return "ns1:Phone";
            if (name == "phoneNumbers")
                return ".//ns1:PhoneRecords//ns1:PhoneRecord";
            if (name == "name")
                return ".//ns1:Name";
            if (name == "lastChTime")
                return "LastChangeTime";
            //if (name == "notes")
            //    return ".//ns2:Note";
            if (callType == "Note" && name == "text")
                return ".//ns2:Note//ns2:Text";
            if (callType == "Tag" && name == "text")
                return ".//ns2:Tag";
            if (name == "tagsString")
                throw new ArgumentException("'tagsString' not supported");
            if (callType == "AddressRecord" && name == "id")
                return "IdRecord";
            if (callType == "Note" && name == "id")
                return ".//ns2:Note//ns2:IdNote";
            if (callType == "PhoneRecord" && name == "id")
                return "ns1:IdPhoneRecord";
            if (callType == "PhoneNumber" && name == "id")
                return "ns1:IdPhoneNumber";

            // В том случае если не было найдено совподеный 
            // вернуть пустую строку
            return "";
        }


        private static Expression StripQuotes(Expression e)
        {
            while (e.NodeType == ExpressionType.Quote)
            {
                e = ((UnaryExpression)e).Operand;
            }
            return e;
        }

        #endregion

    }
}
