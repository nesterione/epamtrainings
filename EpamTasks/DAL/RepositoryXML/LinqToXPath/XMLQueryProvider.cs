﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml;

namespace DAL.RepositoryXML.LinqToXPath
{
    public class XMLQueryProvider : IQueryProvider
    {
        XmlDocument document;

        public XMLQueryProvider(XmlDocument document)
        {
            this.document = document;
        }

        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
        {
            return new QueryableXMLData<TElement>(this, expression);
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            Type elementType = TypeSystem.GetElementType(expression.Type);
            try
            {
                return (IQueryable)Activator.CreateInstance(typeof(QueryableXMLData<>).MakeGenericType(elementType), new object[] { this, expression });
            }
            catch (TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        TResult IQueryProvider.Execute<TResult>(Expression expression)
        {
            bool IsEnumerable = (typeof(TResult).Name == "IEnumerable`1");
            return  (TResult)Convert.ChangeType(XMLQueryContext.Execute(expression, IsEnumerable, document), typeof(TResult) );
        }

        object IQueryProvider.Execute(Expression expression)
        {
            return XMLQueryContext.Execute(expression, true, document);
        }
    }
}
