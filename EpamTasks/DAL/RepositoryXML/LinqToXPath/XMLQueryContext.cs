﻿using System;
using System.Linq.Expressions;
using System.Xml;
using System.Xml.XPath;

namespace DAL.RepositoryXML.LinqToXPath
{
    class XMLQueryContext
    {
        //Создание класса контекста запроса
        internal static object Execute(Expression expression, bool IsEnumerable, XmlDocument document)
        {
            // The expression must represent a query over the data source.
            if (!IsQueryOverDataSource(expression))
                throw new InvalidProgramException("No query over the data source was specified.");

            string XPath = (new QueryTranslator()).Translate(expression);
            XPathNavigator navigator = document.CreateNavigator();

            XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
            manager.AddNamespace("ns1", "namespace1");
            manager.AddNamespace("ns2", "namespace2");

            Type elementType = TypeSystem.GetElementType(expression.Type);
            
            if (IsEnumerable)
                return AddressRepositoryXMLContext.ConvertNodesToList(document.SelectNodes(XPath,manager)); 
            else
                if (navigator.Evaluate(XPath, manager).GetType().Name == "XPathSelectionIterator")
                {
                    XmlNode addressRecordNode = document.SelectSingleNode(XPath, manager);

                    if (addressRecordNode == null)
                        throw new InvalidOperationException();
                    else
                        return AddressRepositoryXMLContext.ConvertNodeToAddressRecord(addressRecordNode);
                }
                else
                {
                    return navigator.Evaluate(XPath, manager);
                }

                
        }

        private static bool IsQueryOverDataSource(Expression expression)
        {
            return (expression is MethodCallExpression);
        }
    }
}
