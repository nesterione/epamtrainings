﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddressBook;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace DAL.RepositoryMemory
{
    public class AddressRepositoryMemory:IRepository<AddressRecord, Guid>
    {
        private string binaryRepositoryPath;
        public List<AddressBook.AddressRecord> addresses { get; set; }

        public AddressRepositoryMemory(string inBinaryRepositoryPath)
        {
            this.binaryRepositoryPath = inBinaryRepositoryPath;
            try
            {
                FileStream fileStream = new FileStream(binaryRepositoryPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                addresses = (List<AddressRecord>)binaryFormatter.Deserialize(fileStream);
                fileStream.Close();
            }
            catch { throw; }
        }

        public List<AddressRecord> GetAll()
        {
            return addresses;
        }

        public void Add(AddressRecord entity)
        {
            addresses.Add(entity);
            SaveChange();
        }

        public void Delete(AddressRecord entity)
        {
            for (int i = 0; i < addresses.Count; i++)
            {
                addresses.Remove(entity);
            }
            SaveChange();
        }

        public void Update(AddressRecord entity)
        {
            bool find = false;
            
            for (int i = 0; i < addresses.Count; i++)
            {
                if (addresses[i].id == entity.id)
                {
                    addresses[i].firstName = entity.firstName;
                    addresses[i].secondName = entity.secondName;
                    addresses[i].notes = entity.notes;
                    addresses[i].phoneNumbers = entity.phoneNumbers;
                    find = true;
                    break;
                }
            }
            if (!find)
                throw new Exception(Properties.Resource.exp_noneItem);
            SaveChange();
        }

        private void SaveChange()
        {
            FileStream fileStream = new FileStream(binaryRepositoryPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, addresses);
            fileStream.Close();
        }

        IQueryable<AddressRecord> IRepository<AddressRecord, Guid>.Get()
        {
           return addresses.AsQueryable<AddressRecord>();
        }
    }
}