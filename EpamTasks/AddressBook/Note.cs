﻿using System;
using System.Collections.Generic;

namespace AddressBook
{
    [Serializable()]
    public class Note
    {
        public Guid id { get; private set; }
        private string _text;
        private List<Tag> _tags;

        const int MAX_LENGHT_NOTES = 200;

        public string text
        {
            get { return _text; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(Properties.Resource.exp_textNoteNull);
                if (value.Length <= 0)
                    throw new Exception(Properties.Resource.exp_textNoteShort);
                if (value.Length > MAX_LENGHT_NOTES)
                    throw new Exception(Properties.Resource.exp_textNoteLong);
                _text = value;
            }
        }

        public List<Tag> tags
        {
            get { return _tags; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();
                else
                {
                    _tags = value;
                }
            }
        }

        public string tagsString
        {
            get
            {
                string rezult = "";
                foreach (Tag tag in tags)
                    rezult += tag + " ";
                return rezult;
            }
        }

        public Note(string text, params Tag[] tags)
        {
            if (text == null)
                throw new ArgumentNullException();

            this.id = Guid.NewGuid();
            this.text = text;
            this.tags = new List<Tag>();
            foreach (Tag tag in tags)
                this.tags.Add(tag);
        }

        public Note(Guid id, string text, params Tag[] tags)
        {
            if (text == null)
                throw new ArgumentNullException();

            this.id = id;
            this.text = text;
            this.tags = new List<Tag>();
            foreach (Tag tag in tags)
                this.tags.Add(tag);
        }
    }
}
