﻿using System;

namespace AddressBook
{
    /// <summary>
    /// Содержит запись номер телефона
    /// </summary>
    [Serializable()]
    public class PhoneRecord
    {
        private PhoneNumber _phoneNumber;

        const int MAX_NAME_LEN = 20;
        const int MIN_NAME_LEN = 3;

        private string _name;
        public Guid id { get; private set; }

        public string name
        {
            get { return _name; }
            private set 
            {
                if (value == null)
                    throw new NullReferenceException();
                if (value.Length > MAX_NAME_LEN)
                    throw new Exception(Properties.Resource.exp_namePhoneLong);
                if (value.Length < MIN_NAME_LEN)
                    throw new Exception(Properties.Resource.exp_namePhoneShort);
                _name = value;
            }
        }

        public PhoneNumber phoneNumber
        {
            get { return _phoneNumber; }
            set 
            {
                if (value == null)
                    throw new Exception(Properties.Resource.exp_phoneNull);
                _phoneNumber = value;
            }
        }

        public PhoneRecord(string name, PhoneNumber phoneNumber)
        {
            try
            {
                this.id = Guid.NewGuid();
                this.name = name;
                this.phoneNumber = phoneNumber;
            }
            catch { throw; }
        }

        public PhoneRecord(Guid id, string name, PhoneNumber phoneNumber)
        {
            try
            {
                this.id = id;
                this.name = name;
                this.phoneNumber = phoneNumber;
            }
            catch { throw; }
        }
    }
}
