﻿using System;

namespace AddressBook
{
    /// <summary>
    /// Преднозначен для хранения телефонного номера
    /// в международном формате
    /// </summary>
   [Serializable()]
    public class PhoneNumber
    {

        public Guid id { get; private set; }
        /// <summary>
        /// Код страны
        /// </summary>
        private int _countryCode;
        /// <summary>
        /// Номер телефона с кодом региона
        /// </summary>
        private long _phone;

        public int countryCode
        {

            get { return _countryCode; }
            private set
            {
                if (value <= 0)
                    throw new Exception(Properties.Resource.exp_negativeCountry);
                if (value > MAX_COUNTRY_CODE)
                    throw new Exception(Properties.Resource.exp_longCountry);

                _countryCode = value;
            }
        }

        public long phone
        {
            get { return _phone; }
            private set
            {
                if (value < 0)
                    throw new Exception(Properties.Resource.exp_negativePhone);
                if (value <= MIN_PHONE)
                    throw new Exception(Properties.Resource.exp_shortPhone);
                if (value > MAX_PHONE)
                    throw new Exception(Properties.Resource.exp_longPhone);

                _phone = value;
            }
        }

        /// <summary>
        /// На данный момент максимальный код страны 
        /// содерит 4 цифры
        /// </summary>
        const int MAX_COUNTRY_CODE = 9999;

        /// <summary>
        /// Номер телефона включает и код региона,
        /// Ввиду большого разнообразия корректный длин кодов региона
        /// и номеров телефонов для различных стран, проверка для 
        /// корректности для каждой страны не выполняеться,
        /// максимальная длина ограничена 16 цифрами
        /// </summary>
        const long MAX_PHONE = 9999999999999999;
        const int MIN_PHONE = 2;

        public PhoneNumber(int countryCode, long phone)
        {
            try
            {
                this.id = Guid.NewGuid();
                this.countryCode = countryCode;
                this.phone = phone;
            }
            catch (Exception ex) { throw ex; }
        }

        public PhoneNumber(Guid id,int countryCode, long phone)
        {
            try
            {
                this.id = id;
                this.countryCode = countryCode;
                this.phone = phone;
            }
            catch (Exception ex) { throw ex; }
        }

        public override string ToString()
        {
            return String.Format("+{0} {1}", countryCode, phone);
        }
    }
}
