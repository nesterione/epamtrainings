﻿using System;

namespace AddressBook
{
    [Serializable()]
    public class Tag
    {
        public string text { get; private set; }

        public Tag(string tag)
        {
            if (tag != null)
            {
                text = tag;
            }
        }

        public override string ToString()
        {
            return text;
        }
    }
}
