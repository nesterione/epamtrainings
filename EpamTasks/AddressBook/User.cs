﻿using System;

namespace AddressBook
{
    /// <summary>
    /// Хранит логин, пароль и роль пользователя
    /// </summary>
    [Serializable()]
    public class User
    {
        public string Login { get; private set; }
        public string Password { get; private set; }
        public UserRoles Role { get; private set; }


        public User(string Login, string Password, UserRoles Role)
        {
            this.Login = Login;
            this.Password = Password;
            this.Role = Role;
        }
    }

    /// <summary>
    /// Перечисление с ролями пользователей
    /// </summary>
    public enum UserRoles {Admin, User }
}
