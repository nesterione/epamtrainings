﻿using System;
using System.Collections.Generic;

namespace AddressBook
{
    /// <summary>
    /// Предназначен для хранения записи адресса
    /// </summary>
   [Serializable()]
    public class AddressRecord
    {
        const int MIN_LENGHT_FIRST_NAME = 1;
        const int MIN_LENGHT_SECOND_NAME = 1;
        

        //static Guid global_id;

        /// <summary>
        /// Идентификационный номер
        /// </summary>
        public Guid id { get; private set; }

        /// <summary>
        /// Имя
        /// </summary>
        private string _firstName;

        /// <summary>
        /// Фамилия
        /// </summary>
        private string _secondName;

        /// <summary>
        /// Заметки
        /// </summary>
        private List<Note> _notes;

        /// <summary>
        /// Время последнего изменения записи
        /// </summary>
        private DateTime _lastChTime;

        /// <summary>
        /// Телефонный номер в международном формате
        /// </summary>
        private List<PhoneRecord> _phoneNumbers;

        /// <summary>
        /// Имя не более 30 символов
        /// </summary>
        public string firstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                if (value.Length < MIN_LENGHT_FIRST_NAME)
                    throw new Exception(Properties.Resource.exp_firstNameShort);
                _firstName = value;
                ChangeTime();
            }
        }

        /// <summary>
        /// Имя не белее 30 символов
        /// </summary>
        public string secondName
        {
            get
            {
                return _secondName;
            }
            set
            {
                if (value.Length < MIN_LENGHT_SECOND_NAME)
                    throw new Exception(Properties.Resource.exp_secondNameShort);
                _secondName = value;
                ChangeTime();
            }
        }

        public List<Note> notes
        {
            get
            {
                return _notes;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException();    
                _notes = value;
                ChangeTime();
            }
        }

        public List<PhoneRecord> phoneNumbers
        {
            get
            {
                return _phoneNumbers;
            }
            set
            {
                _phoneNumbers = value;
                ChangeTime();
            }
        }

        public DateTime lastChTime
        {
            get
            {
                return _lastChTime.ToLocalTime();
            }
            private set
            {
                _lastChTime = value;
            }
        }

        /// <summary>
        /// Изменяет свойство последнего изменения
        /// </summary>
        private void ChangeTime()
        {
            lastChTime = DateTime.UtcNow;
        }

        /// <summary>
        /// Присваивание Id
        /// </summary>
        private void GetId()
        {
            id = Guid.NewGuid();
        }

        /// <summary>
        /// Создает объет с автоматически получаемым Id и временем изменения
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="secondName"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="notes"></param>
        public AddressRecord(string firstName,
                       string secondName,
                       List<PhoneRecord> phoneNumbers,
                       List<Note> notes)
        {
            try
            {
                GetId();
                this.firstName = firstName;
                this.secondName = secondName;

                this.phoneNumbers = new List<PhoneRecord>();
                foreach (PhoneRecord phoneNumber in phoneNumbers)
                    this.phoneNumbers.Add(phoneNumber);

                this.notes = new List<Note>();
                foreach(Note note in notes)
                    this.notes.Add(note);

                ChangeTime();
            }
            catch (Exception ex) { throw ex; };
        }

        /// <summary>
        /// Создает объект с явно заданным Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        /// <param name="secondName"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="notes"></param>
        public AddressRecord(
                       Guid id,
                       string firstName,
                       string secondName,
                       List<PhoneRecord> phoneNumbers,
                       List<Note> notes)
        {
            try
            {
                this.id = id;
                this.firstName = firstName;
                this.secondName = secondName;

                this.phoneNumbers = new List<PhoneRecord>();
                foreach (PhoneRecord phoneNumber in phoneNumbers)
                    this.phoneNumbers.Add(phoneNumber);

                this.notes = new List<Note>();
                foreach (Note note in notes)
                    this.notes.Add(note);

                ChangeTime();
            }
            catch (Exception ex) { throw ex; }
        }

        /// <summary>
        /// Создает объект с явно заданным Id и временем изменения
        /// </summary>
        /// <param name="id"></param>
        /// <param name="firstName"></param>
        /// <param name="secondName"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="notes"></param>
        public AddressRecord(Guid id,
                       string firstName,
                       string secondName,
                        DateTime lastChangeTime,
                       List<PhoneRecord> phoneNumbers,
                       List<Note> notes)
        {
            try
            {
                this.id = id;
                this.firstName = firstName;
                this.secondName = secondName;

                this.phoneNumbers = new List<PhoneRecord>();
                foreach (PhoneRecord phoneNumber in phoneNumbers)
                    this.phoneNumbers.Add(phoneNumber);

                this.notes = new List<Note>();
                foreach (Note note in notes)
                    this.notes.Add(note);

                lastChTime = lastChangeTime;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
